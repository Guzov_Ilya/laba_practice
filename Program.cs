﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_book
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, Directory> directories = new Dictionary<int, Directory>();
            int id = 0;
            bool q = true;
            while (q)
            {
                Console.WriteLine("Здравствуйте!");
                Console.WriteLine("Вы вошли в меню телефонной книжки.");
                Console.WriteLine("1. Создать новую запись.");
                Console.WriteLine("2. Редактировать запись.");
                Console.WriteLine("3. Удалить запись.");
                Console.WriteLine("4. Просмотр созданных записей.");
                Console.WriteLine("5. Просмотр всех созданных записей с краткой информацией: фамилия, имя, номер телефона.");
                Console.WriteLine("6. Выход из программы.");
                Console.WriteLine();
                int caseSwitch = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                switch (caseSwitch)
                {
                    case 1:
                        {
                            Console.WriteLine("Введите фамилию, имя, отчество, страну, номер телефона, дату рождения, организацию, должность и прочие заметки.");
                            directories[id] =  new Directory(Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), long.Parse(Console.ReadLine()), Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), Console.ReadLine());
                            id++;
                            Console.Clear();
                            break;
                        }
                    case 2:
                        {
                           Console.Clear();
                            Console.WriteLine("Все ваши записи. Выберите id записи, которую вы хотите отредактировать");
                            Console.WriteLine();
                            int h = 0;
                            foreach (Directory box in directories.Values)
                            {
                                Console.WriteLine("Запись c id " + h + ":");
                                Console.WriteLine("Фамилия: " + box.surname);
                                Console.WriteLine("Имя: " + box.name);
                                Console.WriteLine("Отчество: " + box.otch);
                                Console.WriteLine("Номер телефона: " + box.phoneNumber);
                                Console.WriteLine("Страна: " + box.country);
                                Console.WriteLine("Дата рождения: " + box.birthDay);
                                Console.WriteLine("Организация: " + box.institution);
                                Console.WriteLine("Должность: " + box.position);
                                Console.WriteLine("Прочие заметки: " + box.notes);
                                Console.WriteLine();
                                h++;
                            }
                            h = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();
                            bool flag = true;
                            while (flag)
                            {
                                Console.WriteLine("1. Изменить имя.");
                                Console.WriteLine("2. Изменить фамилию.");
                                Console.WriteLine("3. Изменить отчество.");
                                Console.WriteLine("4. Изменить страну.");
                                Console.WriteLine("5. Изменить дату рождения.");
                                Console.WriteLine("6. Изменить номер телефона.");
                                Console.WriteLine("7. Изменить организацию.");
                                Console.WriteLine("8. Изменить должность.");
                                Console.WriteLine("9. Изменить прочие заметки.");
                                Console.WriteLine("10. Закончить изменения, вернуться в главное меню и продолжить");
                                Console.WriteLine();
                                int caseSwitchin = Convert.ToInt32(Console.ReadLine());
                                switch (caseSwitchin)
                                {
                                    case 1:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новое имя");
                                            directories.ElementAt(h).Value.name = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 2:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новую фамилию");
                                            directories.ElementAt(h).Value.surname = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 3:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новое отчество");
                                            directories.ElementAt(h).Value.otch = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 4:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новую страну");
                                            directories.ElementAt(h).Value.country = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 5:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новую дату рождения");
                                            directories.ElementAt(h).Value.birthDay = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 6:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новый номер телефона");
                                            directories.ElementAt(h).Value.phoneNumber = long.Parse(Console.ReadLine());
                                            Console.Clear();
                                            break;
                                        }
                                    case 7:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новую организацию");
                                            directories.ElementAt(h).Value.institution = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 8:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новую должность");
                                            directories.ElementAt(h).Value.position = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 9:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Введите новые заметки");
                                            directories.ElementAt(h).Value.notes = Console.ReadLine();
                                            Console.Clear();
                                            break;
                                        }
                                    case 10:
                                        {
                                            Console.Clear();
                                            flag = false;
                                            break;
                                        }
                                }
                            }
                            break;
                        }
                    case 3:
                        {
                            Console.Clear();
                            Console.WriteLine("Все ваши записи. Выберите id записи, которую вы хотите удалить");
                            Console.WriteLine();
                            int h = 0;
                            foreach (Directory box in directories.Values)
                            {
                                Console.WriteLine("Запись c id " + h + ":");
                                Console.WriteLine("ФИО: " + box.surname + " " + box.name + " " + box.otch);
                                Console.WriteLine("Номер телефона: " + box.phoneNumber);
                                Console.WriteLine("Страна: " + box.country);
                                Console.WriteLine("Дата рождения: " + box.birthDay);
                                Console.WriteLine("Организация: " + box.institution);
                                Console.WriteLine("Должность: " + box.position);
                                Console.WriteLine("Прочие заметки: " + box.notes);
                                Console.WriteLine();
                                h++;
                            }
                            h = Convert.ToInt32(Console.ReadLine());
                            directories.Remove(h);
                            Console.WriteLine();
                            Console.WriteLine("Ваша запись успешно удалена. Нажмите любую клавишу, чтобы вернуться в главное меню и продолжить");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }
                    case 4:
                        {
                            int h = 0;
                            Console.Clear();
                            Console.WriteLine("Все ваши записи:");
                            Console.WriteLine();
                            foreach (Directory box in directories.Values)
                            {
                                Console.WriteLine("Запись c id " + h + ":");
                                Console.WriteLine("Фамилия: " + box.surname);
                                Console.WriteLine("Имя: " + box.name);
                                Console.WriteLine("Номер телефона: " + box.phoneNumber);
                                Console.WriteLine();
                                h++;
                            }
                            Console.WriteLine("Введите id записи, которую вы хотите просмотреть");
                            h = Convert.ToInt32(Console.ReadLine());
                            Console.Clear();
                            Console.WriteLine("Запись с id " + h +":");
                            Console.WriteLine();
                            Console.WriteLine("ФИО: " + directories.ElementAt(h).Value.surname + " " + directories.ElementAt(h).Value.name + " " + directories.ElementAt(h).Value.otch);
                            Console.WriteLine("Номер телефона: " + directories.ElementAt(h).Value.phoneNumber);
                            Console.WriteLine("Страна: " + directories.ElementAt(h).Value.country);
                            Console.WriteLine("Дата рождения: " + directories.ElementAt(h).Value.birthDay);
                            Console.WriteLine("Организация: " + directories.ElementAt(h).Value.institution);
                            Console.WriteLine("Должность: " + directories.ElementAt(h).Value.position);
                            Console.WriteLine("Прочие заметки: " + directories.ElementAt(h).Value.notes);
                            Console.WriteLine();
                            Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в главное меню и продолжить");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }
                    case 5:
                        {
                            Console.Clear();
                            Console.WriteLine("Все ваши записи:");
                            Console.WriteLine();
                            foreach (Directory box in directories.Values)
                            {
                                Console.WriteLine("Фамилия: " + box.surname);
                                Console.WriteLine("Имя: " + box.name);
                                Console.WriteLine("Номер телефона: " + box.phoneNumber);
                                Console.WriteLine();
                            }
                            Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в главное меню и продолжить");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }
                    case 6:
                        {
                            q = false;
                            break;
                            
                        }
                }
            }
        }
    }
}

class Directory
{
    public string name, surname, otch, country, birthDay, institution, position, notes;
    public long phoneNumber;

    public Directory(string surname, string name, string otch, string country, long phoneNumber, string birthDay, string institution, string position, string notes)
    {
        this.name = name; this.surname = surname; this.phoneNumber = phoneNumber; this.birthDay = birthDay; this.country = country; this.notes = notes; this.position = position; this.institution = institution; this.otch = otch;
    }
}


